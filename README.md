# Battleship Game
A .NET Core command line battleship game

## Installation & Run
```bash
# Download this project
git clone git@bitbucket.org:giamba/battleship.git
```

```bash
# Build and Run
cd battleship

dotnet build

dotnet run --project Battleship.Console/Battleship.Console.csproj
```

## Tests
```bash
# Run the Tests
dotnet test --logger:"console;verbosity=detailed"
```