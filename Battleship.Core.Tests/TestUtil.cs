using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Battleship.Core.Models;
using Xunit.Abstractions;

namespace Battleship.Core.Tests
{
    public class TestUtil
    {
        public static bool CompareListOfShip(List<Ship> aList, List<Ship> bList)
        {
            for (int i = 0; i < aList.Count; i++)
            {
                if (!(aList[i].StartPoint.Equals(bList[i].StartPoint)) || (aList[i].Type != bList[i].Type) || !(aList[i].EndPoint.Equals(bList[i].EndPoint)))
                    return false;
            }
            return true;
        }
    }
}