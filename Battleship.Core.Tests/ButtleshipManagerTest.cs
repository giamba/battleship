using System.Linq;
using Autofac;
using Battleship.Core.Interfaces;
using Battleship.Core.Ioc;
using Battleship.Core.Models;
using Xunit;
using Xunit.Abstractions;

namespace Battleship.Core.Tests
{
    public class ButtleshipManagerTest : IClassFixture<ContainerFixture>
    {
        private readonly IButtleshipManager _buttleshipManager;

        public ButtleshipManagerTest(ContainerFixture fixture)
        {
            _buttleshipManager = fixture.Container.Resolve<IButtleshipManager>();
        }

        [Fact]
        public void InitShouldWork()
        {
            //arrange 
            var expectedBattleship = 1;
            var expectedDestroyer = 2;

            //act
            _buttleshipManager.Init();

            //assert
            int numOfBattleship = _buttleshipManager.GameStatus.ListOfShip.Where(s => s.Type == ShipType.Battleship).Count();
            int numOfDestroyer = _buttleshipManager.GameStatus.ListOfShip.Where(s => s.Type == ShipType.Destroyer).Count();

            Assert.Equal(expectedBattleship, numOfBattleship);
            Assert.Equal(expectedDestroyer, numOfDestroyer);
        }
    }
}