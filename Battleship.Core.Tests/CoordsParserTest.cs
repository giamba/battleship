using System.Collections.Generic;
using System.Drawing;
using Autofac;
using Battleship.Core.Interfaces;
using Battleship.Core.Ioc;
using Xunit;

namespace Battleship.Core.Tests
{
    public class CoordsParserTest : IClassFixture<ContainerFixture>
    {
        private readonly ICoordsParser _coordsParser;

         public static IEnumerable<object[]> ValidData =>
            new List<object[]>
            {
                new object[] { "A0",  new Point(0, 0)},
                new object[] { "A1",  new Point(0, 1)},
                new object[] { "A2",  new Point(0, 2)},
                new object[] { "A3",  new Point(0, 3)},
                new object[] { "A4",  new Point(0, 4)},
                new object[] { "A5",  new Point(0, 5)},
                new object[] { "A6",  new Point(0, 6)},
                new object[] { "A7",  new Point(0, 7)},
                new object[] { "A8",  new Point(0, 8)},
                new object[] { "A9",  new Point(0, 9)},

                new object[] { "J0",  new Point(9, 0)},
                new object[] { "J1",  new Point(9, 1)},
                new object[] { "J2",  new Point(9, 2)},
                new object[] { "J3",  new Point(9, 3)},
                new object[] { "J4",  new Point(9, 4)},
                new object[] { "J5",  new Point(9, 5)},
                new object[] { "J6",  new Point(9, 6)},
                new object[] { "J7",  new Point(9, 7)},
                new object[] { "J8",  new Point(9, 8)},
                new object[] { "J9",  new Point(9, 9)},        
            };


        public CoordsParserTest(ContainerFixture fixture)
        {
            _coordsParser = fixture.Container.Resolve<ICoordsParser>();
        }

        [Fact]
        void GivenEmptyCoords_Then_Parse_SholdReturnError()
        {   
            //arrange
            string strCoords= "";
            Point expected = new Point(-1, -1); //error

            //act
            var result = _coordsParser.Parse(strCoords);

            //assert
            Assert.Equal(expected, result);
        }
        
        [Theory]
        [InlineData("A")]
        [InlineData("B")]
        [InlineData("C")]
        [InlineData("D")]
        void GivenTooShortCoords_Then_Parse_SholdReturnError(string coords)
        {
            //arrange
             Point expected = new Point(-1, -1); //error

            //act
            var result = _coordsParser.Parse(coords);

            //assert
            Assert.Equal(expected, result);
        }
            
        [Theory]
        [InlineData("A0X")]
        [InlineData("AAAA")]
        [InlineData("B3X")]
        [InlineData("B3XCFYJ")]
        void GivenTooLongCoorde_Then_Parse_SholdReturnError(string coords)
        {
            //arrange
             Point expected = new Point(-1, -1); //error

            //act
            var result = _coordsParser.Parse(coords);

            //assert
            Assert.Equal(expected, result);
        }


        [Theory]
        [InlineData("L1")]
        [InlineData("K1")]
        void GivenOutOfIndexCoords_Then_Parse_SholdReturnError(string coords)
        {
            //arrange
             Point expected = new Point(-1, -1); //error

            //act
            var result = _coordsParser.Parse(coords);

            //assert
            Assert.Equal(expected, result);
        }        

        [Theory]
        [MemberData(nameof(ValidData))]
        void GivenValidCoords_Then_Parse_ShouldWork(string coords, Point expected)
        {
            //act
            var result = _coordsParser.Parse(coords);

            //assert
            Assert.Equal(expected, result);
        }  
    }
}