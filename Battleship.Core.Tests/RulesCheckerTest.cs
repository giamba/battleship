using System.Collections.Generic;
using System.Drawing;
using Autofac;
using Battleship.Core.Interfaces;
using Battleship.Core.Ioc;
using Battleship.Core.Models;
using Xunit;

namespace Battleship.Core.Tests
{
    public class RulesCheckerTest : IClassFixture<ContainerFixture>
    {
        IRulesChecker _rulesChecker;

        public RulesCheckerTest(ContainerFixture fixture)
        {
            _rulesChecker = fixture.Container.Resolve<IRulesChecker>();
        }

        [Fact]
        void GivenAListOfShipAndPointsWithNoCollision_Then_CheckForCollision_ShouldReturnNoCollosion()
        {
            Point startPoint = new Point(0, 0);
            Point endPoint = new Point(0, 5);
            var direction = 1; //east
            var listOfShip = new List<Ship>{};

            //act
            var result = _rulesChecker.CheckForCollision(startPoint, endPoint, direction, listOfShip);

            //assert
            Assert.False(result);
        }

        [Fact]
        void GivenANonEmptyListOfShipAndPointsWithCollision_Then_CheckForCollision_ShouldReturnCollision()
        {
            Point startPoint = new Point(0, 0);
            Point endPoint = new Point(0, 5);
            var direction = 1; //east
            var ship = new Ship(ShipType.Battleship, new Point(0, 3), new Point(0, 7));
            var listOfShip = new List<Ship> { ship };

            //act
            var result = _rulesChecker.CheckForCollision(startPoint, endPoint, direction, listOfShip);

            //assert
            Assert.True(result);
        }

        [Theory]
        [InlineData(0, -1)]
        [InlineData(-1, 0)]
        [InlineData(-1, -1)]
        void GivenANegativesIndex_Then_CheckIfInField_ShouldFail(int rowIndex, int columIndex)
        {
            //arrange
            Point endPoint = new Point(rowIndex, columIndex);

            //act
            var result = _rulesChecker.CheckIfInField(endPoint, 10);

            //assert
            Assert.False(result);
        }

        [Theory]
        [InlineData(10, 0)] //max index 9
        [InlineData(0, 10)]
        void GivenLargersIndex_Then_CheckIfInField_ShouldFail(int rowIndex, int columIndex)
        {
            //arrange
            Point endPoint = new Point(rowIndex, columIndex);
            var buttleshipFieldSize = 9;

            //act
            var result = _rulesChecker.CheckIfInField(endPoint, buttleshipFieldSize);

            //assert
            Assert.False(result);
        }
    }
}