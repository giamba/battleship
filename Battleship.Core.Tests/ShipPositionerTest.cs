using System;
using System.Collections.Generic;
using System.Drawing;
using Autofac;
using Battleship.Core.Interfaces;
using Battleship.Core.Ioc;
using Battleship.Core.Models;
using Xunit;
using Xunit.Abstractions;

namespace Battleship.Core.Tests
{
    public class ShipPositionerTest : IClassFixture<ContainerFixture>
    {
        IShipPositioner _shipPositioner;

        public static IEnumerable<object[]> CornerTestData1 =>
          new List<object[]>
          {
                new object[] { 10,                   //fieldsize 
                               new Point(0, 0),      //startpoint
                               0,                    //direction 0 = north
                               ShipType.Battleship,  //shiptype
                               new List<Ship>{new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4))}  //expected
                              },
          };
        public static IEnumerable<object[]> CornerTestData2 =>
        new List<object[]>
        {
                new object[] { 10,                   //fieldsize 
                               new Point(0, 9),      //startpoint
                               0,                    //direction 0 = north
                               ShipType.Battleship,  //shiptype
                               new List<Ship>{new Ship(ShipType.Battleship, new Point(0, 9), new Point(4, 9))}  //expected
                              },
        };

        public static IEnumerable<object[]> CornerTestData3 =>
        new List<object[]>
        {
                new object[] { 10,                   //fieldsize 
                               new Point(9, 9),      //startpoint
                               1,                    //direction 1 = east
                               ShipType.Battleship,  //shiptype
                               new List<Ship>{new Ship(ShipType.Battleship, new Point(9, 9), new Point(9, 5))}  //expected
                              },
        };
        public static IEnumerable<object[]> CornerTestData4 =>
        new List<object[]>
        {
                new object[] { 10,                   //fieldsize 
                               new Point(9, 0),      //startpoint
                               3,                    //direction 3 = west
                               ShipType.Battleship,  //shiptype
                               new List<Ship>{new Ship(ShipType.Battleship, new Point(9, 0), new Point(5, 0))}  //expected
                              },
        };

        public ShipPositionerTest(ContainerFixture fixture)
        {
            _shipPositioner = fixture.Container.Resolve<IShipPositioner>();
        }

        [Theory]
        [MemberData(nameof(CornerTestData1))]
        [MemberData(nameof(CornerTestData2))]
        [MemberData(nameof(CornerTestData3))]
        [MemberData(nameof(CornerTestData4))]
        public void GivenCornerPoints_Then_PlaceShip_ShouldWork(int fieldSize, Point startPoint, int direction, ShipType shipType, List<Ship> expectedListOfShip)
        {
            //arrange
            List<Ship> listOfShip = new List<Ship>();

            //act
            var result = _shipPositioner.PlaceShip(startPoint, direction, shipType, fieldSize, listOfShip);

            //assert
            Assert.True(result);
            Assert.Equal(1, (int)expectedListOfShip.Count);
            Assert.True(TestUtil.CompareListOfShip(listOfShip, expectedListOfShip));
        }


        [Fact]
        void GivenAStartPointWithEstCollision_Then_PlaceShip_ShouldPlaceTheShipTowardSouth()
        {   //- - - - - - - - - -
            //X 0 5 5 5 5 5 0 0 0  X = start point
            //0 0 0 0 0 0 0 0 0 0  (0,2) (0,7) //initial

            //arrange
            Point startPoint = new Point(0, 0);
            int direction = 0; //north  
            ShipType shipType = ShipType.Battleship;
            int fieldSize = 10;
            List<Ship> listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 2), new Point(0, 7)) };

            var expected = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(4, 0)) };

            //act
            var result = _shipPositioner.PlaceShip(startPoint, direction, shipType, fieldSize, listOfShip);
        }

        [Fact]
        public void GivenAStartPointWithSouthCollision_Then_PlaceShip_ShouldPlaceTheShipTowardEast()
        {   //- - - - - - - - - -
            //X 0 0 0 0 0 0 0 0 0  X = start point
            //5 5 5 5 5 0 0 0 0 0  (1,0) (1,4) //initial

            //arrange 
            var startPoint = new Point(0, 0);
            int direction = 2; //sud
            ShipType shipType = ShipType.Battleship;
            int fieldSize = 10;
            var initialListOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(1, 0), new Point(1, 4)) };

            var expected = new List<Ship>{new Ship(ShipType.Battleship, new Point(1, 0) , new Point(1,4)),
                                           new Ship(ShipType.Battleship, new Point(0, 0) , new Point(0,4))};

            //act
            var result = _shipPositioner.PlaceShip(startPoint, direction, shipType, fieldSize, initialListOfShip);

            //assert
            Assert.True(TestUtil.CompareListOfShip(initialListOfShip, expected));
        }
    }
}