using System.Collections.Generic;
using System.Drawing;
using Battleship.Core.Utils;
using Xunit;

namespace Battleship.Core.Tests
{
    public class PointUtilTest
    {
        public static IEnumerable<object[]> HorizontalData =>
            new List<object[]>
            {
                new object[] { new Point(0, 0), new Point(3, 0), new List<Point> { new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(3, 0) } },
                new object[] { new Point(3, 2), new Point(3, 5), new List<Point> { new Point(3, 2), new Point(3, 3), new Point(3, 4), new Point(3, 5) } }, 
            };

        public static IEnumerable<object[]> VerticalData =>
            new List<object[]>
            {
                new object[] { new Point(0, 0), new Point(0, 3), new List<Point> { new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, 3) } }, 
                new object[] { new Point(1, 4), new Point(4, 4), new List<Point> { new Point(1, 4), new Point(2, 4), new Point(3, 4), new Point(4, 4) } }, 
            };

        [Theory]
        [MemberData(nameof(HorizontalData))]
        void GivenTwoPointsAndAVerticalHorientation_Then_GetAllPoints_ShouldWork(Point startPoint, Point endPoint, List<Point> expected)
        {  
            //act
            var result = PointUtil.GetAllPoints(startPoint, endPoint);

            //assert
            Assert.Equal(expected, result);
        }

        [Theory]
        [MemberData(nameof(VerticalData))]
        void GivenTwoPointsAndAHorizontalOrientation_Then_GetAllPoints_ShouldWork(Point startPoint, Point endPoint, List<Point> expected)
        {
            //act
            var result = PointUtil.GetAllPoints(startPoint, endPoint);

            //assert
            Assert.Equal(expected, result);
        }
    }
}