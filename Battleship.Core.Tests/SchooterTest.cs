using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Autofac;
using Battleship.Core.Interfaces;
using Battleship.Core.Ioc;
using Battleship.Core.Models;
using Xunit;

namespace Battleship.Core.Tests
{
    public class SchooterTest : IClassFixture<ContainerFixture>
    {
        IShooter _shooter;

        public SchooterTest(ContainerFixture fixture)
        {
            _shooter = fixture.Container.Resolve<IShooter>();
        }

        [Fact]
        void GivenAShotNotInTarget_Then_Shot_ShouldReturnMiss()
        {
            //arrange
            var shotPoint = new Point(0, 5);
            var listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4)) };

            //act
            var result = _shooter.Shot(shotPoint, listOfShip);

            //assert
            Assert.Equal(ShooterResult.Missed, result);
        }

        [Fact]
        void GivenOneShotInTarget_Then_Shot_ShouldReturnHitted()
        {
            //arrange
            var shotPoint = new Point(0, 0);
            var listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4)) };

            //act
            var result = _shooter.Shot(shotPoint, listOfShip);

            //assert
            Assert.Equal(ShooterResult.Hitted, result);
            Assert.Equal(1, (int) listOfShip.ElementAt(0).HittedPoints.Count);
        }

        [Fact]
        void GivenTwoShotInTarget_Then_Shot_ShouldReturnHitted()
        {
            //arrange
            var shotPointOne = new Point(0, 0);
            var shotPointTwo = new Point(0, 1);
            var listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4)) };

            //act
            var resultFirstShot = _shooter.Shot(shotPointOne, listOfShip);
            var resultSecondShot = _shooter.Shot(shotPointTwo, listOfShip);

            //assert
            Assert.Equal(ShooterResult.Hitted, resultFirstShot);
            Assert.Equal(ShooterResult.Hitted, resultSecondShot);
            Assert.Equal(2, (int) listOfShip.ElementAt(0).HittedPoints.Count);
        }

        [Fact]
        void GivenFiveShotInTargetForABattleship_Then_Shot_ShouldReturnSinked()
        {
            //arrange
            var shotPointOne = new Point(0, 0);
            var shotPointTwo = new Point(0, 1);
            var shotPointThree = new Point(0, 2);
            var shotPointFour = new Point(0, 3);
            var shotPointFive = new Point(0, 4);
            var listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4)) };

            //act
            var resultFirstShot = _shooter.Shot(shotPointOne, listOfShip);
            var resultSecondShot = _shooter.Shot(shotPointTwo, listOfShip);
            var resultThirdShot = _shooter.Shot(shotPointThree, listOfShip);
            var resultFourthShot = _shooter.Shot(shotPointFour, listOfShip);
            var resultFifthShot = _shooter.Shot(shotPointFive, listOfShip);

            //assert
            Assert.Equal(ShooterResult.Hitted, resultFirstShot);
            Assert.Equal(ShooterResult.Hitted, resultSecondShot);
            Assert.Equal(ShooterResult.Hitted, resultThirdShot);
            Assert.Equal(ShooterResult.Hitted, resultFourthShot);

            Assert.Equal(ShooterResult.Sinked, resultFifthShot);


            Assert.Equal(5, (int) listOfShip.ElementAt(0).HittedPoints.Count);
        }

        [Fact]
        void GivenASinkedShipHittedAgain_Then_Shot_ShouldReturnMissed()
        {
            //arrange
            var shotPointOne = new Point(0, 0);
            var shotPointTwo = new Point(0, 1);
            var shotPointThree = new Point(0, 2);
            var shotPointFour = new Point(0, 3);
            var shotPointFive = new Point(0, 4);
            var listOfShip = new List<Ship> { new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4)) };

            //act
            var resultFirstShot = _shooter.Shot(shotPointOne, listOfShip);
            var resultSecondShot = _shooter.Shot(shotPointTwo, listOfShip);
            var resultThirdShot = _shooter.Shot(shotPointThree, listOfShip);
            var resultFourthShot = _shooter.Shot(shotPointFour, listOfShip);
            var resultFifthShot = _shooter.Shot(shotPointFive, listOfShip);

            var resultSixthShotAlreadySinked = _shooter.Shot(shotPointFive, listOfShip);

            //assert
            Assert.Equal(ShooterResult.Hitted, resultFirstShot);
            Assert.Equal(ShooterResult.Hitted, resultSecondShot);
            Assert.Equal(ShooterResult.Hitted, resultThirdShot);
            Assert.Equal(ShooterResult.Hitted, resultFourthShot);
            Assert.Equal(ShooterResult.Sinked, resultFifthShot);

            Assert.Equal(ShooterResult.Missed, resultSixthShotAlreadySinked); //missed ship was sinked

            Assert.Equal(5, (int) listOfShip.ElementAt(0).HittedPoints.Count);
        }

        [Fact]
        void GivenTwoBattleshipShipAndTenHits_Then_Shot_ShouldReturnBothSinked()
        {
            //arrange
            var shotPointOne1 = new Point(0, 0);
            var shotPointTwo1 = new Point(0, 1);
            var shotPointThree1 = new Point(0, 2);
            var shotPointFour1 = new Point(0, 3);
            var shotPointFive1 = new Point(0, 4);

            var shotPointOne2 = new Point(1, 0);
            var shotPointTwo2 = new Point(1, 1);
            var shotPointThree2 = new Point(1, 2);
            var shotPointFour2 = new Point(1, 3);
            var shotPointFive2 = new Point(1, 4);


            var ship1 = new Ship(ShipType.Battleship, new Point(0, 0), new Point(0, 4));
            var ship2 = new Ship(ShipType.Battleship, new Point(1, 0), new Point(1, 4));

            var listOfShip = new List<Ship> { ship1, ship2 };

            //act
            var resultFirstShot1 = _shooter.Shot(shotPointOne1, listOfShip);
            var resultSecondShot1 = _shooter.Shot(shotPointTwo1, listOfShip);
            var resultThirdShot1 = _shooter.Shot(shotPointThree1, listOfShip);
            var resultFourthShot1 = _shooter.Shot(shotPointFour1, listOfShip);
            var resultFifthShot1 = _shooter.Shot(shotPointFive1, listOfShip);

            var resultFirstShot2 = _shooter.Shot(shotPointOne2, listOfShip);
            var resultSecondShot2 = _shooter.Shot(shotPointTwo2, listOfShip);
            var resultThirdShot2 = _shooter.Shot(shotPointThree2, listOfShip);
            var resultFourthShot2 = _shooter.Shot(shotPointFour2, listOfShip);
            var resultFifthShot2 = _shooter.Shot(shotPointFive2, listOfShip);

            Assert.Equal(5, (int) listOfShip.ElementAt(0).HittedPoints.Count);
            Assert.Equal(5, (int) listOfShip.ElementAt(1).HittedPoints.Count);
        }
    }
}