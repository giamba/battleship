using System.Collections.Generic;
using System.Drawing;

namespace Battleship.Core.Models
{
    public enum ShipType
    {
        Battleship = 5,
        Destroyer = 4,
    }

    public class Ship
    {
        public Ship(ShipType type, Point startPoint, Point endPoint)
        {
            Type = type;
            StartPoint = startPoint;
            EndPoint = endPoint;
            HittedPoints = new List<Point>();
        }

        public ShipType Type { get; }
        public Point StartPoint { get; }
        public Point EndPoint { get; }
        public List<Point> HittedPoints { get; }
        public bool Sinked { get; set; }
    }
}