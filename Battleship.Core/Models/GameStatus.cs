using System.Collections.Generic;
using System.Linq;

namespace Battleship.Core.Models
{
    public class GameStatus
    {
        public List<Ship> ListOfShip { get; }

        public GameStatus()
        {
            ListOfShip = new List<Ship>();
        }
    }
}