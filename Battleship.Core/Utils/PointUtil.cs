using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Battleship.Core.Models;

namespace Battleship.Core.Utils
{
    public class PointUtil
    {
        private enum Orientation
        {
            Horizontal,
            Vertical
        }

        //Return all the points between two Points
        static public List<Point> GetAllPoints(Point startPoint, Point endPoint)
        {
            Orientation orientation = Orientation.Horizontal;

            if (startPoint.Y == endPoint.Y)
                orientation = Orientation.Vertical;

            List<Point> retList = new List<Point>();

            if (orientation == Orientation.Vertical)
            {
                var sortedPoints = new List<Point> { startPoint, endPoint }.OrderByDescending(p => p.X).ToList();
                //Biggest - Lower
                var distance = sortedPoints.ElementAt(0).X - sortedPoints.ElementAt(1).X;
                for (int i = 0, x = sortedPoints.ElementAt(1).X; i <= distance; i++, x++)
                {
                    Point point = new Point(x, startPoint.Y);
                    retList.Add(point);
                }
            }

            if (orientation == Orientation.Horizontal)
            {
                var sortedPoints = new List<Point> { startPoint, endPoint }.OrderByDescending(p => p.Y).ToList();
                //Biggest - Lower
                var distance = sortedPoints.ElementAt(0).Y - sortedPoints.ElementAt(1).Y;
                for (int i = 0, y = sortedPoints.ElementAt(1).Y; i <= distance; i++, y++)
                {
                    Point point = new Point(startPoint.X, y);
                    retList.Add(point);
                }
            }
            return retList;
        }

        static public List<Point> GetAllGameStatusPoints(List<Ship> listOfShip)
        {
            List<Point> retList = new List<Point>();
            foreach (var s in listOfShip)
            {
                var listOfPoint = GetAllPoints(s.StartPoint, s.EndPoint);
                retList = retList.Concat(listOfPoint).ToList();
            }
            return retList;
        }

        static public bool CheckCollision(List<Point> listA, List<Point> listB)
        {
            foreach (var a in listA)
            {
                foreach (var b in listB)
                {
                    if ((a.X == b.X) && (a.Y == b.Y))
                        return true;
                }
            }
            return false;
        }

        static public Point CalulateEndPoint(Point startPoint, int direction, int battleshipSize)
        {
            Point endPoint = new Point();

            if (direction == 0) //north
            {
                endPoint.X = startPoint.X - (battleshipSize - 1);
                endPoint.Y = startPoint.Y;
            }
            if (direction == 1) //east
            {
                endPoint.X = startPoint.X;
                endPoint.Y = startPoint.Y + (battleshipSize - 1);
            }
            if (direction == 2) //south
            {
                endPoint.X = startPoint.X + (battleshipSize - 1);
                endPoint.Y = startPoint.Y;
            }
            if (direction == 3) //west
            {
                endPoint.X = startPoint.X;
                endPoint.Y = startPoint.Y - (battleshipSize - 1);
            }
            return endPoint;
        }
    }
}