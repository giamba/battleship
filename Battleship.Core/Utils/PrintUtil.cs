using System.Collections.Generic;
using System.Text;

namespace Battleship.Core.Utils
{
    public class PrintUtil
    {
        public static string GetGameFieldAsString(int[,] matrix)
        {
            int rowLength = matrix.GetLength(0);
            int colLength = matrix.GetLength(1);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < rowLength; i++)
            {
                var rowStr = "";
                for (int j = 0; j < colLength; j++)
                {
                    rowStr += string.Format("{0} ", matrix[i, j]);
                }
                sb.Append(rowStr + "\n");
            }
            return AddHeaderAndColumns(sb.ToString());
        }

        public static string GetHittedGameFieldAsString(int[,] hittedMatrix)
        {
            int rowLength = hittedMatrix.GetLength(0);
            int colLength = hittedMatrix.GetLength(1);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < rowLength; i++)
            {
                var rowStr = "";
                for (int j = 0; j < colLength; j++)
                {
                    if (hittedMatrix[i, j] == -1)
                        rowStr += "X ";
                    else
                        rowStr += hittedMatrix[i, j].ToString() + " ";
                }
                sb.Append(rowStr + "\n");
            }
            return AddHeaderAndColumns(sb.ToString());
        }

        static string AddHeaderAndColumns(string tableStr)
        {
            var letterDict = new Dictionary<int, string>{
                {0, "A"},{1, "B"},{2, "C"},{3, "D"},{4, "E"},
                {5, "F"},{6, "G"},{7, "H"},{8, "I"},{9, "J"}
                };

            //add row letters
            StringBuilder sb = new StringBuilder();
            var splitMatrix = tableStr.Split('\n');
            for (int i = 0; i < splitMatrix.Length - 1; i++)
            {
                sb.Append(letterDict[i] + " - " + splitMatrix[i] + "\n");
            }

            //add header
            var fieldWithHeather = "    0 1 2 3 4 5 6 7 8 9\n" +
                                   "    - - - - - - - - - -\n"
            + sb.ToString();
            return fieldWithHeather;
        }
    }
}