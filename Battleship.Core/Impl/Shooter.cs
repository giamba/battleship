using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Battleship.Core.Interfaces;
using Battleship.Core.Models;
using Battleship.Core.Utils;

namespace Battleship.Core.Impl
{
    public class Shooter : IShooter
    {
        public ShooterResult Shot(Point p, List<Ship> listOfShip)
        {
            foreach (var ship in listOfShip.Where(s => !s.Sinked))
            {
                if (HasAlreadyBeenShooted(ship, p)) return ShooterResult.Hitted;

                List<Point> points = PointUtil.GetAllPoints(ship.StartPoint, ship.EndPoint);
                if (points.Contains(p))
                {
                    ship.HittedPoints.Add(p);
                    if (IsSinked(ship))
                    {
                        ship.Sinked = true;
                        return ShooterResult.Sinked;
                    }
                    else
                        return ShooterResult.Hitted;
                }
            }
            return ShooterResult.Missed;
        }

        bool HasAlreadyBeenShooted(Ship ship, Point p)
        {
            if (ship.HittedPoints.Contains(p))
                return true;

            return false;
        }

        bool IsSinked(Ship ship)
        {
            var shipLength = (int)ship.Type;
            if (ship.HittedPoints.Count == shipLength)
                return true;

            return false;
        }
    }
}