using System.Collections.Generic;
using System.Drawing;
using Battleship.Core.Interfaces;

namespace Battleship.Core.Impl
{
    public class CoordsParser : ICoordsParser
    {
        private readonly Dictionary<char, int> charDict = new Dictionary<char, int>
        {
            {'A', 0},
            {'B', 1},
            {'C', 2},
            {'D', 3},
            {'E', 4},
            {'F', 5},
            {'G', 6},
            {'H', 7},
            {'I', 8},
            {'J', 9}
        };

        public Point Parse(string coords)
        {
            coords = coords.ToUpper();
            
            var retPoint = new Point(-1, -1);
            if (string.IsNullOrEmpty(coords) || coords.Length == 1 || coords.Length > 2)
                return retPoint;

            var x = coords.ToCharArray()[0];
            var y = coords.ToCharArray()[1];

            if (!charDict.ContainsKey(x))
                return retPoint;
            else
                return new Point(charDict[x], (int) (y - '0'));
        }
    }
}