using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Battleship.Core.Interfaces;
using Battleship.Core.Models;
using Battleship.Core.Utils;

namespace Battleship.Core.Impl
{
    public class RulesChecker : IRulesChecker
    {

        public bool CheckIfInField(Point endPoint, int buttleshipFieldSize)
        {
            if ((endPoint.X >= 0 && endPoint.Y >= 0) && (endPoint.X < buttleshipFieldSize && endPoint.Y < buttleshipFieldSize))
                return true;

            return false;
        }

        public bool CheckForCollision(Point startPoint, Point endPoint, int direction, List<Ship> listOfShip)
        {
            //no collision
            if (listOfShip.Count == 0)
                return false;

            List<Point> points = new List<Point>();
            if (direction == 0 || direction == 2) //vertical 
            {
                points = PointUtil.GetAllPoints(startPoint, endPoint);
            }
            if (direction == 3 || direction == 1) //horizontal 
            {
                points = PointUtil.GetAllPoints(startPoint, endPoint);
            }

            var pointsInGame = PointUtil.GetAllGameStatusPoints(listOfShip);

            if(PointUtil.CheckCollision(points, pointsInGame))
                return true;                

            return false;
        }
    }
}