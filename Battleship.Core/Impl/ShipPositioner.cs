using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Battleship.Core.Interfaces;
using Battleship.Core.Models;
using Battleship.Core.Utils;

namespace Battleship.Core.Impl
{
    public class ShipPositioner : IShipPositioner
    {
        IRulesChecker _rulesChecker;
        public ShipPositioner(IRulesChecker rulesChecker)
        {
            _rulesChecker = rulesChecker;
        }

        //Given a random point and a random direction try to write a ship in the first space available
        //trying all the direction 0=noth, 1=est, 2=south, 3=west 
        public bool PlaceShip(Point startPoint, int direction, ShipType shipType, int fielSize, List<Ship> listOfShip)
        {
            int directionCounter = 0;
            while (directionCounter < 4)
            {
                if (TryToWriteShip(startPoint, direction % 4, shipType, fielSize, listOfShip))
                    return true;
                else
                {
                    direction++; 
                    directionCounter++;
                }
            }
            return false;
        }

        private bool TryToWriteShip(Point startPoint, int direction, ShipType shipType, int buttleshipFieldSize, List<Ship> listOfShip)
        {
            //calculate the end point coords 
            var endPoint = PointUtil.CalulateEndPoint(startPoint, direction, (int)shipType);

            //check if the ship fit in the field
            if (_rulesChecker.CheckIfInField(endPoint, buttleshipFieldSize) != true)
                return false;

            //check for collision
            if (_rulesChecker.CheckForCollision(startPoint, endPoint, direction, listOfShip) == true)
                return false;

            listOfShip.Add(new Ship(shipType, startPoint, endPoint));
            return true;
        }  
    }
}