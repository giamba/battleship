using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Battleship.Core.Interfaces;
using Battleship.Core.Models;
using Battleship.Core.Utils;

namespace Battleship.Core.Impl
{

    //todo create GameSettings like:
    /* public static class GameSettings
    {
        public static int FieldSize => 10; // in this way we dont need to pass the field down to the ShipPositioner and RulesChecker
        public static int maxAttemps => 50;
    }*/


    public class ButtleshipManager : IButtleshipManager
    {
        private const int FieldSize = 10;

        public GameStatus _gameStatus;

        public GameStatus GameStatus { get { return _gameStatus; } }

        private IShipPositioner _shipPositioner;

        private IShooter _shooter;

        private ICoordsParser _coordsParser;

        private IRulesChecker _rulesChecker;

        public ButtleshipManager(IShipPositioner shipPositioner, IShooter shooter, ICoordsParser coordsParser, IRulesChecker rulesChecker)
        {
            _shipPositioner = new ShipPositioner(rulesChecker);
            _shooter = shooter;
            _coordsParser = coordsParser;
            _rulesChecker = rulesChecker;
        }

        public void Init()
        {
            _gameStatus = new GameStatus();
            PlaceRandomShip(ShipType.Battleship, 1);
            PlaceRandomShip(ShipType.Destroyer, 2);
        }

        //todo: I dont like    
        public string Shot(string coords)
        {
            var parserResult = _coordsParser.Parse(coords);
            if (parserResult == new Point(-1, -1))
                return "coords_not_valid";

            var shotResult = _shooter.Shot(parserResult, _gameStatus.ListOfShip);

            if (shotResult == ShooterResult.Hitted)
                return "Hitted";
            else if (shotResult == ShooterResult.Missed)
                return "Missed";
            else if (shotResult == ShooterResult.Sinked)
                return "Sinked";
            else
                return "coords_not_valid";
        }

        public bool IsGameFinished()
        {
            return _gameStatus.ListOfShip.All(s => s.Sinked);
        }

        private void PlaceRandomShip(ShipType shipType, int quantity)
        {
            int attemp = 0;
            int shipInserted = 0;
            while (shipInserted < quantity || attemp == 50)
            {
                attemp++;
                var startPoint = RandomStartPoint();
                var direction = RandomDirection();
                var result = _shipPositioner.PlaceShip(startPoint, direction, shipType, FieldSize, _gameStatus.ListOfShip);
                if (result)
                    shipInserted++;
            }
        }

        private Point RandomStartPoint()
        {
            Random random = new Random();
            int startX = random.Next(0, FieldSize - 1);
            int startY = random.Next(0, FieldSize - 1);
            return new Point(startX, startY);
        }

        private int RandomDirection()
        {
            Random random = new Random();
            return random.Next(0, 3); //0=north, 1=east, 2=south, 3=west 
        }

        public string GetGameFieldAsString()
        {
            return PrintUtil.GetGameFieldAsString(GetGameFieldAsMatrix());
        }

        public string GetHittedGameFieldAsString()
        {
            return PrintUtil.GetHittedGameFieldAsString(GetHittedGameFieldAsMatrix());
        }

        public int[,] GetGameFieldAsMatrix()
        {
            return MakeGameMatrix(_gameStatus.ListOfShip);
        }

        public int[,] GetHittedGameFieldAsMatrix()
        {
            return MakeHittedGameMatrix(_gameStatus.ListOfShip);
        }

        //todo: maybe better to create only one Method whith the type of the matrix that ew want to generate
        //todo: MakeGameMatrix(List<Ship> listOfShip, MatrixType type)
        int[,] MakeGameMatrix(List<Ship> listOfShip)
        {
            var buttleshipField = new int[FieldSize, FieldSize]; // default all zeros

            foreach (var s in listOfShip)
            {
                var points = PointUtil.GetAllPoints(s.StartPoint, s.EndPoint);

                foreach (var p in points)
                {
                    if (s.Type == ShipType.Battleship)
                        buttleshipField[p.X, p.Y] = 5;  //todo: just 1 is enough
                    else if (s.Type == ShipType.Destroyer)
                        buttleshipField[p.X, p.Y] = 4;  //todo: just 1 is enough
                }
            }
            return buttleshipField;
        }

        int[,] MakeHittedGameMatrix(List<Ship> listOfShip)
        {
            var buttleshipField = new int[FieldSize, FieldSize]; // default all zeros

            var totalHittedPoints = new List<Point>();
            foreach (var s in listOfShip)
                totalHittedPoints = totalHittedPoints.Concat(s.HittedPoints).ToList();

            foreach (var p in totalHittedPoints)
                buttleshipField[p.X, p.Y] = -1;

            return buttleshipField;
        }
    }
}