using System.Drawing;
using Battleship.Core.Models;

namespace Battleship.Core.Interfaces
{
    public interface IButtleshipManager
    {
        void Init();

        string Shot(string coords);

        bool IsGameFinished();

        string GetGameFieldAsString();

        string GetHittedGameFieldAsString();

        int[,] GetGameFieldAsMatrix();

        int[,] GetHittedGameFieldAsMatrix();

        GameStatus GameStatus { get; }
    }
}