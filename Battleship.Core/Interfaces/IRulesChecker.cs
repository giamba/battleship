using System.Collections.Generic;
using System.Drawing;
using Battleship.Core.Models;

namespace Battleship.Core.Interfaces
{
    public interface IRulesChecker
    {
        bool CheckIfInField(Point endPoint, int buttleshipFieldSize);
        bool CheckForCollision(Point startPoint, Point endPoint, int direction, List<Ship> listOfShip);
    }
}