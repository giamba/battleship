using System.Collections.Generic;
using System.Drawing;
using Battleship.Core.Models;

namespace Battleship.Core.Interfaces
{
    public interface IShipPositioner    
    {
        bool PlaceShip(Point startPoint, int direction, ShipType shipType, int fieldSize, List<Ship> listOfShip);
    }
}