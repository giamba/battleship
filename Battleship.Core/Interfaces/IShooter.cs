using System.Collections.Generic;
using System.Drawing;
using Battleship.Core.Models;

namespace Battleship.Core.Interfaces
{
   public enum ShooterResult
    {
        Hitted,
        Missed,
        Sinked
    }

    public interface IShooter
    {
         ShooterResult Shot(Point p, List<Ship> listOfShip);
    }
}