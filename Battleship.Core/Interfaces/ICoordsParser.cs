using System.Drawing;

namespace Battleship.Core.Interfaces
{
    public interface ICoordsParser
    {
        Point Parse(string coords);
    }
}