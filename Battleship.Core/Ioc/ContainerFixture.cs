using System;
using Autofac;

namespace Battleship.Core.Ioc
{
    public class ContainerFixture : IDisposable
    {
        public IContainer Container { get; private set; }

        public ContainerFixture()
        {
           Container = Bootstrapper.BuildContainer();
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}