using Autofac;
using Battleship.Core.Impl;
using Battleship.Core.Interfaces;

namespace Battleship.Core.Ioc
{
    public class Bootstrapper
    {

        public static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<RulesChecker>().As<IRulesChecker>();
            builder.RegisterType<ShipPositioner>().As<IShipPositioner>();
            builder.RegisterType<Shooter>().As<IShooter>();
            builder.RegisterType<ButtleshipManager>().As<IButtleshipManager>();
            builder.RegisterType<CoordsParser>().As<ICoordsParser>();
            return builder.Build();
        }
    }
}