namespace Battleship.Console
{
    using System;
    using SysConsole = System.Console; //for macos 
    using Battleship.Core.Interfaces;
    using Battleship.Core.Impl;

    class Application
    {
        protected readonly IButtleshipManager _buttleshipManager;

        public Application(IButtleshipManager buttleshipManager)
        {
            _buttleshipManager = buttleshipManager;
        }

        public void Run()
        {
            string cmd = String.Empty;
            SysConsole.WriteLine("This is a battleship game for console commands");

            do
            {
                SysConsole.Write("(type start for a new game) :>");
                cmd = SysConsole.ReadLine().ToString().ToLower();

                switch (cmd)
                {
                    case "start":
                        _buttleshipManager.Init();
                        Console.WriteLine("############################################");
                        Console.WriteLine("################ SOLUTION ##################");
                        Console.WriteLine("############################################");
                        SysConsole.WriteLine(_buttleshipManager.GetGameFieldAsString());
                        Console.WriteLine("############################################");
                        Console.WriteLine();

                        while (!_buttleshipManager.IsGameFinished())
                        {
                            Console.WriteLine("Current Status:");
                            SysConsole.WriteLine(_buttleshipManager.GetHittedGameFieldAsString());
                            Console.WriteLine("Enter coords:");
                            string coords = SysConsole.ReadLine().ToString();

                            var shotResult = _buttleshipManager.Shot(coords);
                            if (shotResult == "coords_not_valid")
                            {
                                Console.WriteLine("Coords not valid!");
                            }
                            else
                            {
                                Console.WriteLine("Shot result: " + shotResult);
                                Console.WriteLine();
                            }
                        }
                        Console.WriteLine("Congratulations you've finished the game!!!");
                        break;
                    default:
                        SysConsole.WriteLine("Bad Command");
                        break;

                }
            } while (cmd.ToLower() != "exit");
        }
    }
}