﻿namespace Battleship.Console
{
    using Autofac;
    using Battleship.Core.Impl;
    using Battleship.Core.Interfaces;

    class Program
    {

        private static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Application>();
            builder.RegisterType<ButtleshipManager>().As<IButtleshipManager>();
            builder.RegisterType<ShipPositioner>().As<IShipPositioner>();
            builder.RegisterType<Shooter>().As<IShooter>();
            builder.RegisterType<CoordsParser>().As<ICoordsParser>();
            builder.RegisterType<RulesChecker>().As<IRulesChecker>();

            return builder.Build();
        }

        static void Main(string[] args)
        {
            CompositionRoot().Resolve<Application>().Run();          
        }
    }
}
    
